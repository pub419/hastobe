import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { ChargeDetailModule } from '../src/charge-detail/charge-detail.module';
import { ApiOutput, Cdr, Rate } from '../src/Shared/infrastructure';
import { ChargeDetailService } from '../src/charge-detail/charge-detail.service';
import { Console } from 'console';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!');
  });
});

describe('ChargeDetailController (e2e)', () => {
  let app: INestApplication;
  let result = {
    overall: 7.04,
    components: {
      energy: 3.277,
      time: 2.767,
      transaction: 1,
    },
  };

  let cdr: Cdr = {
    meterStart: 1204307,
    timestampStart: new Date(2021, 4, 5, 10, 4),
    meterStop: 1215230,
    timestampStop: new Date(2021, 4, 5, 11, 27),
  };
  let rate: Rate = { energy: 0.3, time: 2, transaction: 1 };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [ChargeDetailModule, ChargeDetailService],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());

    await app.init();
  });

  it('/getChargeDetailRate (POST) 400 without body', async () => {
    const response = await request(app.getHttpServer()).post(
      '/charge-detail/getChargeDetailRate',
    );

    expect(response.statusCode).toBe(400);
    expect(response.body.error).toBe('Bad Request');
  });

  it('/getChargeDetailRate (POST) 400 null cdr', async () => {
    const response = await request(app.getHttpServer())
      .post('/charge-detail/getChargeDetailRate')
      .send({ cdr: null, rate });

    expect(response.statusCode).toBe(400);
    expect(response.body.error).toBe('Bad Request');
  });

  it('/getChargeDetailRate (POST) 400 null rate', async () => {
    const response = await request(app.getHttpServer())
      .post('/charge-detail/getChargeDetailRate')
      .send({ cdr, rate: null });

    expect(response.statusCode).toBe(400);
    expect(response.body.error).toBe('Bad Request');
  });

  it('/getChargeDetailRate (POST) 400 not valid type meterStart', async () => {
    const response = await request(app.getHttpServer())
      .post('/charge-detail/getChargeDetailRate')
      .send({ cdr: { ...cdr, meterStart: 'string' }, rate });

    expect(response.statusCode).toBe(400);
    expect(response.body.error).toBe('Bad Request');
  });

  it('/getChargeDetailRate (POST) 400 not valid date format timestampStart', async () => {
    const response = await request(app.getHttpServer())
      .post('/charge-detail/getChargeDetailRate')
      .send({ cdr: { ...cdr, timestampStart: 'string' }, rate });

    expect(response.statusCode).toBe(400);
    expect(response.body.error).toBe('Bad Request');
  });

  it('/getChargeDetailRate (POST) 400 not valid date format timestampStop', async () => {
    const response = await request(app.getHttpServer())
      .post('/charge-detail/getChargeDetailRate')
      .send({ cdr: { ...cdr, timestampStop: null }, rate });

    expect(response.statusCode).toBe(400);
    expect(response.body.error).toBe('Bad Request');
  });

  /***************************************************************************************
   *     Lots of tests to be added here
   *      - test for null cdr.meterStart
   *      - test for null cdr.meterStop
   *      - test for null cdr.timestampStart
   *      - test for null cdr.timestampStop
   *      - test for null rate.energy
   *      - test for null rate.time
   *      - test for null rate.transaction
   *      and so on...
   *    all of them are almost the same and takes time to write them all for now
   **************************************************************************************/

  it('/getChargeDetailRate (POST) 201 in validated parameters', async () => {
    const response = await request(app.getHttpServer())
      .post('/charge-detail/getChargeDetailRate')
      .send({ rate, cdr });
    expect(response.statusCode).toBe(201);
    expect(response.body.error).toBe(undefined);
    expect(response.body).toStrictEqual(result);
  });

  afterAll(async () => {
    await app.close();
  });
});
