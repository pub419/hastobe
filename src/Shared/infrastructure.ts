import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsInt,
  IsISO8601,
  IsNotEmpty,
  IsNotEmptyObject,
  IsNumber,
  Min,
  ValidateNested,
} from 'class-validator';

export class Rate {
  constructor() {
    this.energy = 0;
    this.time = 0;
    this.transaction = 1;
  }

  @IsNumber()
  @Min(0)
  @ApiProperty({ type: Number, required: true, minimum: 0 })
  energy: number;

  @IsNumber()
  @Min(0)
  @ApiProperty({ type: Number, required: true, minimum: 0 })
  time: number;

  @IsNumber()
  @Min(0)
  @ApiProperty({ type: Number, required: true, minimum: 0 })
  transaction: number;
}

export class Cdr {
  @IsInt()
  @Min(0)
  @ApiProperty({ type: Number, required: true, minimum: 0 })
  meterStart: number;

  @IsISO8601()
  @IsNotEmpty()
  @ApiProperty({ type: Date, required: true })
  timestampStart: Date;

  @IsInt()
  @Min(0)
  @ApiProperty({
    type: Number,
    required: true,
    minimum: 0,
    description: 'it should be equal or greater than meterStart',
  })
  meterStop: number;

  @IsISO8601()
  @IsNotEmpty()
  @ApiProperty({
    type: Date,
    required: true,
    description: 'it should be equal or greater than timestampStart',
  })
  timestampStop: Date;
}

export class ApiPayload {
  @IsNotEmptyObject()
  @ApiProperty()
  @ValidateNested()
  @Type(() => Rate)
  rate: Rate;

  @IsNotEmptyObject()
  @ApiProperty()
  @ValidateNested()
  @Type(() => Cdr)
  cdr: Cdr;
}

export class ApiOutput {
  constructor() {
    this.overall = 0;
    this.components = new Rate();
  }
  @IsNumber()
  @Min(0)
  overall: number;
  @IsNotEmptyObject()
  @ValidateNested()
  @Type(() => Rate)
  components: Rate;
}
