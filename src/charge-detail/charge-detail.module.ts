import { Module } from '@nestjs/common';
import { ChargeDetailService } from './charge-detail.service';
import { ChargeDetailController } from './charge-detail.controller';

@Module({
  providers: [ChargeDetailService],
  controllers: [ChargeDetailController]
})
export class ChargeDetailModule {}
