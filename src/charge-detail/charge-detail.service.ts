import { BadRequestException, Injectable } from '@nestjs/common';
import { ApiOutput, ApiPayload, Cdr, Rate } from '../Shared/infrastructure';

@Injectable()
export class ChargeDetailService {
  constructor() {}

  async consumptionCalculator(cdr: Cdr): Promise<Rate> {
    if (
      !cdr ||
      cdr.meterStart == null ||
      cdr.meterStop == null ||
      !cdr.timestampStart ||
      !cdr.timestampStop
    ) {
      throw new BadRequestException('input data should not be null');
    }
    if (
      cdr.meterStart > cdr.meterStop ||
      cdr.timestampStart > cdr.timestampStop
    ) {
      throw new BadRequestException(
        'start values should be less than stop values',
      );
    }
    if (cdr.meterStart < 0 || cdr.meterStop < 0) {
      throw new BadRequestException(
        'meterStart and meterStop should not be negative',
      );
    }

    return {
      energy: (cdr.meterStop - cdr.meterStart) / 1000,
      time:
        (new Date(cdr.timestampStop).getTime() - new Date(cdr.timestampStart).getTime()) /
        1000 / // convert to seconds
        60 / // convert to minutes
        60, //  convert to hours
      transaction: 1,
    };
  }

  async componentsCalculator(rate: Rate, consumption: Rate): Promise<Rate> {
    if (
      !rate ||
      rate.energy == null ||
      rate.energy < 0 ||
      rate.time == null ||
      rate.time < 0 ||
      rate.transaction == null ||
      rate.transaction < 0
    ) {
      throw new BadRequestException(
        'rate parameter should not be null or negative',
      );
    }

    if (
      !consumption ||
      consumption.energy == null ||
      consumption.energy < 0 ||
      consumption.time == null ||
      consumption.time < 0 ||
      consumption.transaction == null ||
      consumption.transaction < 0
    ) {
      throw new Error(
        'consumption is not calculated correctly, please check the input data',
      );
    }

    return {
      energy: rate.energy * consumption.energy,
      time: rate.time * consumption.time,
      transaction: rate.transaction * consumption.transaction,
    };
  }

  async overallCalculator(value: Rate): Promise<ApiOutput> {
    if (
      !value ||
      value.energy == null ||
      value.energy < 0 ||
      value.time == null ||
      value.time < 0 ||
      value.transaction == null ||
      value.transaction < 0
    ) {
      throw new Error(
        'components are not calculated correctly, please check the input data',
      );
    }
    return {
      overall: +(value.energy + value.time + value.transaction).toFixed(2),
      components: {
        energy: +value.energy.toFixed(3),
        time: +value.time.toFixed(3),
        transaction: +value.transaction.toFixed(3),
      },
    };
  }

  async calculateChargeDetail(input: ApiPayload): Promise<ApiOutput> {
    const consumption = await this.consumptionCalculator(input.cdr);
    const value = await this.componentsCalculator(input.rate, consumption);
    return await this.overallCalculator(value);
  }
}
