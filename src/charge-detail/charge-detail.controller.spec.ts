import { Test, TestingModule } from '@nestjs/testing';
import { ApiOutput, ApiPayload } from '../Shared/infrastructure';
import { ChargeDetailController } from './charge-detail.controller';
import { ChargeDetailService } from './charge-detail.service';

describe('ChargeDetailController', () => {
  let controller: ChargeDetailController;
  let service: ChargeDetailService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ChargeDetailController],
      providers: [ChargeDetailService],
    }).compile();

    controller = module.get<ChargeDetailController>(ChargeDetailController);
    service = module.get<ChargeDetailService>(ChargeDetailService);
  });

  describe('Defined', () => {
    it('should be defined', () => {
      expect(controller).toBeDefined();
    });
  });

  describe('getChargeDetail', () => {
    let result: ApiOutput = {
      overall: 0.3,
      components: { energy: 0.1, time: 0.2, transaction: 0.1 },
    };
    it('should return exact service response', async () => {
      jest
        .spyOn(service, 'calculateChargeDetail')
        .mockImplementation(() => Promise.resolve(result));

      expect(await controller.getChargeDetail(new ApiPayload())).toBe(result);
    });
  });
});
