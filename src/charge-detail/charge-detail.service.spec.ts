// @ts-nocheck
import { BadRequestException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { Cdr, Rate } from 'src/Shared/infrastructure';
import { ChargeDetailService } from './charge-detail.service';

describe('ChargeDetailService', () => {
  let service: ChargeDetailService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ChargeDetailService],
    }).compile();

    service = module.get<ChargeDetailService>(ChargeDetailService);
  });
  describe('Defined', () => {
    it('should be defined', () => {
      expect(service).toBeDefined();
    });
  });

  describe('consumptionCalculator', () => {
    let cdr: Cdr = {
      meterStart: 1204307,
      timestampStart: new Date(2021, 4, 5, 10, 4),
      meterStop: 1215230,
      timestampStop: new Date(2021, 4, 5, 11, 27),
    };
    it('should throw error in null value', async () => {
      try {
        expect(await service.consumptionCalculator(null)).toThrow();
      } catch (error) {
        expect(error).toBeInstanceOf(BadRequestException);
        expect(error.message).toBe('input data should not be null');
      }
    });

    it('should throw error in null meterStart', async () => {
      try {
        expect(
          await service.consumptionCalculator({ ...cdr, meterStart: null }),
        ).toThrow();
      } catch (error) {
        expect(error).toBeInstanceOf(BadRequestException);
        expect(error.message).toBe('input data should not be null');
      }
    });

    it('should throw error in null timestampStart', async () => {
      try {
        expect(
          await service.consumptionCalculator({ ...cdr, timestampStart: null }),
        ).toThrow();
      } catch (error) {
        expect(error).toBeInstanceOf(BadRequestException);
        expect(error.message).toBe('input data should not be null');
      }
    });

    it('should throw error in null meterStop', async () => {
      try {
        expect(
          await service.consumptionCalculator({ ...cdr, meterStop: null }),
        ).toThrow();
      } catch (error) {
        expect(error).toBeInstanceOf(BadRequestException);
        expect(error.message).toBe('input data should not be null');
      }
    });

    it('should throw error in null timestampStop', async () => {
      try {
        expect(
          await service.consumptionCalculator({ ...cdr, timestampStop: null }),
        ).toThrow();
      } catch (error) {
        expect(error).toBeInstanceOf(BadRequestException);
        expect(error.message).toBe('input data should not be null');
      }
    });

    it('should throw error in meterStop less than meterStart', async () => {
      try {
        expect(
          await service.consumptionCalculator({
            ...cdr,
            meterStart: cdr.meterStop + 1,
          }),
        ).toThrow();
      } catch (error) {
        expect(error).toBeInstanceOf(BadRequestException);
        expect(error.message).toBe(
          'start values should be less than stop values',
        );
      }
    });

    it('should throw error in timestampStop greater than timestampStart', async () => {
      try {
        expect(
          await service.consumptionCalculator({
            ...cdr,
            timestampStart: cdr.timestampStop.getTime() + 1,
          }),
        ).toThrow();
      } catch (error) {
        expect(error).toBeInstanceOf(BadRequestException);
        expect(error.message).toBe(
          'start values should be less than stop values',
        );
      }
    });

    it('should throw error in negative meterStart', async () => {
      try {
        expect(
          await service.consumptionCalculator({ ...cdr, meterStart: -2 }),
        ).toThrow();
      } catch (error) {
        expect(error).toBeInstanceOf(BadRequestException);
        expect(error.message).toBe(
          'meterStart and meterStop should not be negative',
        );
      }
    });

    it('should throw error in negative meterStop', async () => {
      try {
        expect(
          await service.consumptionCalculator({
            ...cdr,
            meterStart: -10,
            meterStop: -8,
          }),
        ).toThrow();
      } catch (error) {
        expect(error).toBeInstanceOf(BadRequestException);
        expect(error.message).toBe(
          'meterStart and meterStop should not be negative',
        );
      }
    });

    it('should return correct value (challenge #1 test case)', async () => {
      const result: Rate = {
        energy: 10.923,
        time: 1.3833333333333333,
        transaction: 1,
      };
      expect(await service.consumptionCalculator(cdr)).toStrictEqual(result);
    });
  });

  describe('componentsCalculator', () => {
    let rate: Rate = { energy: 0.3, time: 2, transaction: 1 };
    let consumption: Rate = {
      energy: 10.923,
      time: 1.3833333333333333,
      transaction: 1,
    };
    describe('check rate', () => {
      it('should throw error in null rate', async () => {
        try {
          expect(
            await service.componentsCalculator(null, consumption),
          ).toThrow();
        } catch (error) {
          expect(error).toBeInstanceOf(BadRequestException);
          expect(error.message).toBe(
            'rate parameter should not be null or negative',
          );
        }
      });

      it('should throw error in null rate energy value', async () => {
        try {
          expect(
            await service.componentsCalculator(
              { ...rate, energy: null },
              consumption,
            ),
          ).toThrow();
        } catch (error) {
          expect(error).toBeInstanceOf(BadRequestException);
          expect(error.message).toBe(
            'rate parameter should not be null or negative',
          );
        }
      });

      it('should throw error in negative rate energy value', async () => {
        try {
          expect(
            await service.componentsCalculator(
              { ...rate, energy: -2 },
              consumption,
            ),
          ).toThrow();
        } catch (error) {
          expect(error).toBeInstanceOf(BadRequestException);
          expect(error.message).toBe(
            'rate parameter should not be null or negative',
          );
        }
      });

      it('should throw error in null rate time value', async () => {
        try {
          expect(
            await service.componentsCalculator(
              { ...rate, time: null },
              consumption,
            ),
          ).toThrow();
        } catch (error) {
          expect(error).toBeInstanceOf(BadRequestException);
          expect(error.message).toBe(
            'rate parameter should not be null or negative',
          );
        }
      });

      it('should throw error in negative rate time value', async () => {
        try {
          expect(
            await service.componentsCalculator(
              { ...rate, time: -5 },
              consumption,
            ),
          ).toThrow();
        } catch (error) {
          expect(error).toBeInstanceOf(BadRequestException);
          expect(error.message).toBe(
            'rate parameter should not be null or negative',
          );
        }
      });

      it('should throw error in null rate transaction value', async () => {
        try {
          expect(
            await service.componentsCalculator(
              { ...rate, transaction: null },
              consumption,
            ),
          ).toThrow();
        } catch (error) {
          expect(error).toBeInstanceOf(BadRequestException);
          expect(error.message).toBe(
            'rate parameter should not be null or negative',
          );
        }
      });

      it('should throw error in negative rate transaction value', async () => {
        try {
          expect(
            await service.componentsCalculator(
              { ...rate, transaction: -1 },
              consumption,
            ),
          ).toThrow();
        } catch (error) {
          expect(error).toBeInstanceOf(BadRequestException);
          expect(error.message).toBe(
            'rate parameter should not be null or negative',
          );
        }
      });
    });

    describe('check consumption', () => {
      it('should throw error in null consumption', async () => {
        try {
          expect(await service.componentsCalculator(rate, null)).toThrow();
        } catch (error) {
          expect(error).toBeInstanceOf(Error);
          expect(error.message).toBe(
            'consumption is not calculated correctly, please check the input data',
          );
        }
      });

      it('should throw error in null consumption energy', async () => {
        try {
          expect(
            await service.componentsCalculator(rate, {
              ...consumption,
              energy: null,
            }),
          ).toThrow();
        } catch (error) {
          expect(error).toBeInstanceOf(Error);
          expect(error.message).toBe(
            'consumption is not calculated correctly, please check the input data',
          );
        }
      });

      it('should throw error in negative consumption energy', async () => {
        try {
          expect(
            await service.componentsCalculator(rate, {
              ...consumption,
              energy: -2,
            }),
          ).toThrow();
        } catch (error) {
          expect(error).toBeInstanceOf(Error);
          expect(error.message).toBe(
            'consumption is not calculated correctly, please check the input data',
          );
        }
      });

      it('should throw error in null consumption time', async () => {
        try {
          expect(
            await service.componentsCalculator(rate, {
              ...consumption,
              time: null,
            }),
          ).toThrow();
        } catch (error) {
          expect(error).toBeInstanceOf(Error);
          expect(error.message).toBe(
            'consumption is not calculated correctly, please check the input data',
          );
        }
      });

      it('should throw error in negative consumption time', async () => {
        try {
          expect(
            await service.componentsCalculator(rate, {
              ...consumption,
              time: -5,
            }),
          ).toThrow();
        } catch (error) {
          expect(error).toBeInstanceOf(Error);
          expect(error.message).toBe(
            'consumption is not calculated correctly, please check the input data',
          );
        }
      });

      it('should throw error in null consumption transaction', async () => {
        try {
          expect(
            await service.componentsCalculator(rate, {
              ...consumption,
              transaction: null,
            }),
          ).toThrow();
        } catch (error) {
          expect(error).toBeInstanceOf(Error);
          expect(error.message).toBe(
            'consumption is not calculated correctly, please check the input data',
          );
        }
      });

      it('should throw error in negative consumption transaction', async () => {
        try {
          expect(
            await service.componentsCalculator(rate, {
              ...consumption,
              transaction: -1,
            }),
          ).toThrow();
        } catch (error) {
          expect(error).toBeInstanceOf(Error);
          expect(error.message).toBe(
            'consumption is not calculated correctly, please check the input data',
          );
        }
      });
    });

    describe('check rate and consumption', () => {
      it('should return the desired value', async () => {
        const result = {
          energy: 3.2769,
          time: 2.7666666666666666,
          transaction: 1,
        };
        expect(await service.componentsCalculator(rate, consumption)).toEqual(
          result,
        );
      });
    });
  });

  describe('overallCalculator', () => {
    let components = {
      energy: 3.2769,
      time: 2.7666666666666666,
      transaction: 1,
    };
    it('should throw error in null components', async () => {
      try {
        expect(await service.overallCalculator(null)).toThrow();
      } catch (error) {
        expect(error).toBeInstanceOf(Error);
        expect(error.message).toBe(
          'components are not calculated correctly, please check the input data',
        );
      }
    });

    it('should throw error in null components energy', async () => {
      try {
        expect(
          await service.overallCalculator({ ...components, energy: null }),
        ).toThrow();
      } catch (error) {
        expect(error).toBeInstanceOf(Error);
        expect(error.message).toBe(
          'components are not calculated correctly, please check the input data',
        );
      }
    });

    it('should throw error in negative components energy', async () => {
      try {
        expect(
          await service.overallCalculator({ ...components, energy: -12 }),
        ).toThrow();
      } catch (error) {
        expect(error).toBeInstanceOf(Error);
        expect(error.message).toBe(
          'components are not calculated correctly, please check the input data',
        );
      }
    });

    it('should throw error in null components time', async () => {
      try {
        expect(
          await service.overallCalculator({ ...components, time: null }),
        ).toThrow();
      } catch (error) {
        expect(error).toBeInstanceOf(Error);
        expect(error.message).toBe(
          'components are not calculated correctly, please check the input data',
        );
      }
    });

    it('should throw error in negative components time', async () => {
      try {
        expect(
          await service.overallCalculator({ ...components, time: -8 }),
        ).toThrow();
      } catch (error) {
        expect(error).toBeInstanceOf(Error);
        expect(error.message).toBe(
          'components are not calculated correctly, please check the input data',
        );
      }
    });

    it('should throw error in null components transaction', async () => {
      try {
        expect(
          await service.overallCalculator({ ...components, transaction: null }),
        ).toThrow();
      } catch (error) {
        expect(error).toBeInstanceOf(Error);
        expect(error.message).toBe(
          'components are not calculated correctly, please check the input data',
        );
      }
    });

    it('should throw error in negative components transaction', async () => {
      try {
        expect(
          await service.overallCalculator({ ...components, transaction: -3 }),
        ).toThrow();
      } catch (error) {
        expect(error).toBeInstanceOf(Error);
        expect(error.message).toBe(
          'components are not calculated correctly, please check the input data',
        );
      }
    });
    it('should return the desired value', async () => {
      const result = {
        overall: 7.04,
        components: {
          energy: 3.277,
          time: 2.767,
          transaction: 1,
        },
      };
      expect(await service.overallCalculator(components)).toEqual(result);
    });
  });

  describe('calculateChargeDetail', () => {
    let cdr: Cdr = {
      meterStart: 1204307,
      timestampStart: new Date(2021, 4, 5, 10, 4),
      meterStop: 1215230,
      timestampStop: new Date(2021, 4, 5, 11, 27),
    };
    let rate: Rate = { energy: 0.3, time: 2, transaction: 1 };

    it('should return the desired value', async () => {
      const result = {
        overall: 7.04,
        components: {
          energy: 3.277,
          time: 2.767,
          transaction: 1,
        },
      };
      expect(await service.calculateChargeDetail({rate, cdr})).toEqual(result);
    });
  });
});
