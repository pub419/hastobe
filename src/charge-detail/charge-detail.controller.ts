import { Body, Controller, Post } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { ApiOutput, ApiPayload } from '../Shared/infrastructure';
import { ChargeDetailService } from './charge-detail.service';

@Controller('charge-detail')
@ApiTags('charge-detail')
export class ChargeDetailController {
  constructor(private readonly chargeDetailService: ChargeDetailService) {}
  @Post('getChargeDetailRate')
  @ApiBody({
    type: ApiPayload,
  })
  async getChargeDetail(@Body() payload: ApiPayload): Promise<ApiOutput> {
    return await this.chargeDetailService.calculateChargeDetail(payload);
  }
}
