import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ChargeDetailModule } from './charge-detail/charge-detail.module';

@Module({
  imports: [ChargeDetailModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
